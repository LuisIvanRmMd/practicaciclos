﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejemplosdecase
{
    class Program
    {
        static void Main(string[] args)
        {
            int op;
            Console.WriteLine("Problema 1 - Escriba números decimales mientras el número insertado sea mayor al anterior");
            Console.WriteLine("Problema 2 - Ingrese su número de cuenta y contraseña");
            Console.WriteLine("Problema 3 - Tabla de multiplicar del 0 al 10");
            op = Convert.ToInt32(Console.ReadLine());
            if (op == 1)
            {
                ///Punto 1
                ///Números decimales
                double num1, num2;
                num2 = 0;
                Console.WriteLine("Escriba números decimales mientras el número insertado sea mayor al anterior");
                Console.WriteLine("Escribe un número");
                num1 = double.Parse(Console.ReadLine());
                while (num1 > num2)
                {
                    num2 = num1;
                    num1 = double.Parse(Console.ReadLine());
                }
                Console.WriteLine("El número es menor");
                Console.ReadKey();
            }
            if (op == 2)
            {
                ///Punto 2
                ///Usuario y contraseña
                int a, b;
                Console.WriteLine("Ingrese su número de cuenta y contraseña");
                Console.WriteLine("Ingrese su número de cuenta");
                a = Convert.ToInt32(Console.ReadLine());
                while (a != 1024)
                {
                    Console.WriteLine("Ingrese su número de cuenta");
                    a = Convert.ToInt32(Console.ReadLine());
                }
                Console.WriteLine("Ingrese su contraseña");
                b = Convert.ToInt32(Console.ReadLine());
                while (b != 4567)
                {
                    Console.WriteLine("Ingrese su contraseña");
                    b = Convert.ToInt32(Console.ReadLine());
                }
            }

            if (op == 3)
            {
                ///Punto 3
                ///Tabla de multilpicar
                int nm;
                for (nm = 0; nm <= 10; nm++)
                {
                    Console.WriteLine("Tabla de multiplicar del " + nm);
                    Console.WriteLine(1 * nm);
                    Console.WriteLine(2 * nm);
                    Console.WriteLine(3 * nm);
                    Console.WriteLine(4 * nm);
                    Console.WriteLine(5 * nm);
                    Console.WriteLine(6 * nm);
                    Console.WriteLine(7 * nm);
                    Console.WriteLine(8 * nm);
                    Console.WriteLine(9 * nm);
                    Console.WriteLine(10 * nm);
                    Console.ReadKey();
                }
            }

            else
            {
                Console.WriteLine("Fin del programa");
                Console.ReadKey();
            }
        }
    }
}

